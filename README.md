# Deep Learning Assisted Real Time CCTV Face Recognition 

Public surveillance cameras help officials to solve many cases. However, when we are tracking for a person in real-time, it requires more manpower and time. But now there is a rapid advancement in cloud computing. Deep neural networks is gaining traction in the society as many tech giants like Google, Facebook, and Microsoft open source their libraries for the public use. 
So we propose a solution using **Keras and TensorFlow** to track a person in the real-time to save manpower and time. The live video from CCTV camera is fed frame by frame to the model. The faces in the frame are detected using **YOLO** algorithm and sent to the image classifier to recognise the face. If the specified person is identified, it highlights him and alerts are sent to the user. To extend further, an App (Android Application) is developed for updating the database through which the officials can login, feed new images for recognition and monitor the predictions in real-time.

# Android App Screenshots

![Image 1](images/GY 1.png)
![Image 2](images/GY 2.png)
![Image 3](images/GY 3.png)
![Image 4](images/GY 4.png)
![Image 5](images/GY 5.png)
![Image 6](images/GY 6.png)
![Image 7](images/GY 7.png)

