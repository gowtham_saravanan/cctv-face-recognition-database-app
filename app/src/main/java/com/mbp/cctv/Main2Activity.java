package com.mbp.cctv;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.UUID;


public class Main2Activity extends AppCompatActivity {

    private final int PICK_IMAGE_REQUEST = 71;
    final int TAKE_PICTURE = 21;
    List<String> imageList,UIDList;

    FirebaseStorage storage;
    StorageReference storageReference;

    FirebaseDatabase putfirebaseDatabase,getfirebaseDatabase;
    DatabaseReference putdatabaseReference,getdatabaseReference,moddatabaseReference;


    CoordinatorLayout coordinatorLayout;

    public static final String TAG="Main2Activity";
    ImageRecyclerView imageRecyclerView;

    private Uri filePath;
    String user;
    Map<String,String> map;


    RecyclerView recyclerView;
    GridLayoutManager layoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        coordinatorLayout=(CoordinatorLayout)findViewById(R.id.clayout2) ;

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        storage=FirebaseStorage.getInstance();
        storageReference=storage.getReference();
        Bundle b1=getIntent().getExtras();
        user=b1.getString("user");
        Log.i(TAG, "User Selected  "+user);


        recyclerView=(RecyclerView)findViewById(R.id.recyclerView);
        layoutManager=new GridLayoutManager(this,2);

        recyclerView.setLayoutManager(layoutManager);

        getfirebaseDatabase=FirebaseDatabase.getInstance();
        getdatabaseReference=getfirebaseDatabase.getReference("images").child(user);


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                        Intent intent=new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent,"Select Picture"),PICK_IMAGE_REQUEST);

            }
        });


//        FloatingActionButton fab1 = (FloatingActionButton) findViewById(R.id.fab1);
//
//        fab1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                        //Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//                        //startActivityForResult(cameraIntent, TAKE_PICTURE);
//
//
//                Snackbar.make(coordinatorLayout, "Message", Snackbar.LENGTH_LONG).show();
//            }
//        });

        getdatabaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                imageList=new ArrayList<>();
                UIDList=new ArrayList<>();

                for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                    Log.i(TAG,"key = "+dataSnapshot1.getKey());
                    UIDList.add(String.valueOf(dataSnapshot1.getKey()));
                    String img=String.valueOf(dataSnapshot1.child("ImageURL").getValue());
                    //Log.i(TAG,"Images "+String.valueOf(dataSnapshot1.child("ImageURL").getValue()));

                    if(!img.isEmpty() && !img.equals("null")){
                    imageList.add(String.valueOf(dataSnapshot1.child("ImageURL").getValue()));
                }
                }

                imageRecyclerView=new ImageRecyclerView(Main2Activity.this,imageList,UIDList,user);


                recyclerView.setAdapter(imageRecyclerView);
                Log.i(TAG,"List size "+imageList.size());
                
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });






        //ListIterator iterator=imageList.listIterator();






    }

    private Uri getImageUri(Context context, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

@Override
    protected void onActivityResult(int requestCode,int resultCode,Intent data) {
    super.onActivityResult(requestCode,resultCode,data);
    if(requestCode==PICK_IMAGE_REQUEST&&resultCode==RESULT_OK&&data!=null&&data.getData()!=null){
        Log.i(TAG, "onActivityResult: Gallary result");
        filePath=data.getData();
        String s=data.getScheme();
        Log.i(TAG, "onActivityResult: "+s);
        uploadImage();
        try{
            Bitmap bitmap=MediaStore.Images.Media.getBitmap(getContentResolver(),filePath);
           // imageview.setImageBitmap(bitmap);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    if(requestCode==TAKE_PICTURE&&resultCode==RESULT_OK) {





        try {

            Bitmap photo = (Bitmap) data.getExtras().get("data");
            filePath=getImageUri(Main2Activity.this,photo);
            uploadImage();
            Log.i(TAG, "onActivityResult: Picture result "+filePath);
            //imageview.setImageBitmap(photo);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

}

    private void uploadImage() {

        if(filePath != null)
        {
            map= new HashMap<String,String>();
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading...");
            progressDialog.show();

            StorageReference ref = storageReference.child("images/"+ UUID.randomUUID().toString());
            ref.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            progressDialog.dismiss();
                            Toast.makeText(Main2Activity.this, "Uploaded", Toast.LENGTH_SHORT).show();

                            //Log.i(TAG, "onSuccess: "+String.valueOf(taskSnapshot.getMetadata().getName()));





                            taskSnapshot.getMetadata().getReference().getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    Log.i(TAG, "onSuccess: "+String.valueOf(uri));

                                    map.put("ImageURL",String.valueOf(uri));


                                    putfirebaseDatabase=FirebaseDatabase.getInstance();
                                    putdatabaseReference=putfirebaseDatabase.getReference("images").child(user);
                                    String key=putdatabaseReference.push().getKey();
                                    putdatabaseReference.child(key).setValue(map);
                                    moddatabaseReference=putfirebaseDatabase.getReference("modified");
                                    String key1=moddatabaseReference.push().getKey();
                                    moddatabaseReference.child(key1).setValue(user);


                                }
                            });




                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(Main2Activity.this, "Failed "+e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0*taskSnapshot.getBytesTransferred()/taskSnapshot
                                    .getTotalByteCount());
                            progressDialog.setMessage("Uploaded "+(int)progress+"%");
                        }
                    });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_predictions, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent=new Intent(Main2Activity.this,About.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
