package com.mbp.cctv;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

public class ImageRecyclerView extends RecyclerView.Adapter<ImageRecyclerView.recyclerviewholder> {

    Context ct;
    List<String> imagelist,UIDlist;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference modReference,listreference;
    String user;

    public ImageRecyclerView( Context context,List<String> imagelist,List<String> UIDlist,String user) {
        ct=context;
        this.imagelist=imagelist;
        this.UIDlist=UIDlist;
        this.user=user;
    }



    @NonNull
    @Override
    public ImageRecyclerView.recyclerviewholder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater=LayoutInflater.from(ct);
        View v=inflater.inflate(R.layout.recycler_layout,viewGroup,false);
        return new recyclerviewholder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ImageRecyclerView.recyclerviewholder viewHolder, int i) {
        String url=imagelist.get(i);
        Glide.with(ct).load(url).into(viewHolder.im);

        viewHolder.im.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ct,ShowImage.class);
                int pos=viewHolder.getAdapterPosition();
                intent.putExtra("ImageURL",imagelist.get(pos));
                intent.putExtra("ImageUID",UIDlist.get(pos));
                intent.putExtra("User",user);
                ct.startActivity(intent);
            }
        });

        viewHolder.im.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                AlertDialog.Builder alertbuilder = new AlertDialog.Builder(ct);
                alertbuilder.setMessage("Do you want to delete the image");
                alertbuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.w("Main2Activity","Delete");
                        firebaseDatabase=FirebaseDatabase.getInstance();
                        modReference=firebaseDatabase.getReference("modified");
                        String key=modReference.push().getKey();
                        modReference.child(key).setValue(user);
                        listreference=firebaseDatabase.getReference("images").child(user);
                        listreference.child(UIDlist.get(viewHolder.getAdapterPosition())).removeValue();


                    }
                });

                alertbuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.w("Main2Activity","Don't Delete");
                    }
                });

                AlertDialog alertDialog=alertbuilder.create();
                alertDialog.show();
                return true;
            }
        });



    }

    @Override
    public int getItemCount() {
        return imagelist.size();
    }

    public class recyclerviewholder extends RecyclerView.ViewHolder {
        ImageView im;
        public recyclerviewholder(@NonNull View itemView) {

            super(itemView);
            im=(ImageView)itemView.findViewById(R.id.imageView2);
        }
    }
}
