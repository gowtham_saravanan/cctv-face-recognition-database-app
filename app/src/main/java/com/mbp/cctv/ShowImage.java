package com.mbp.cctv;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class ShowImage extends AppCompatActivity {
    FirebaseDatabase firebaseDatabase;
    DatabaseReference modReference,listreference;
    ImageView imageView;
    String ImageURL,UID,user;
    CoordinatorLayout coordinatorLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_image);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        coordinatorLayout=(CoordinatorLayout)findViewById(R.id.clayout3);

        imageView=(ImageView)findViewById(R.id.imageView);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.w("ShowImage","Delete");
                firebaseDatabase=FirebaseDatabase.getInstance();
                modReference=firebaseDatabase.getReference("modified");
                String key=modReference.push().getKey();
                modReference.child(key).setValue(user);
                listreference=firebaseDatabase.getReference("images").child(user);
                listreference.child(UID).removeValue();
//                Snackbar.make(view,"Image deleted succesfully",Snackbar.LENGTH_SHORT).show();


                Snackbar snackbar;
                snackbar = Snackbar.make(coordinatorLayout, "Image deleted succesfully", Snackbar.LENGTH_SHORT);
                View snackBarView = snackbar.getView();
                snackBarView.setBackgroundColor(Color.WHITE);
                snackbar.show();

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                }, 1000);

            }
        });

        Bundle bundle=getIntent().getExtras();
         ImageURL=bundle.getString("ImageURL");
        UID=bundle.getString("ImageUID");
        user=bundle.getString("User");

        Glide.with(ShowImage.this).load(ImageURL).into(imageView);

        imageView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                AlertDialog.Builder alertbuilder = new AlertDialog.Builder(ShowImage.this);
                alertbuilder.setMessage("Do you want to delete the image");
                alertbuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.w("ShowImage","Delete");
                        firebaseDatabase=FirebaseDatabase.getInstance();
                        modReference=firebaseDatabase.getReference("modified");
                        String key=modReference.push().getKey();
                        modReference.child(key).setValue(user);
                        listreference=firebaseDatabase.getReference("images").child(user);
                        listreference.child(UID).removeValue();

//                        Snackbar.make(coordinatorLayout,"Image deleted succesfully",Snackbar.LENGTH_SHORT).show();

                        Snackbar snackbar;
                        snackbar = Snackbar.make(coordinatorLayout, "Image deleted succesfully", Snackbar.LENGTH_SHORT);
                        View snackBarView = snackbar.getView();
                        snackBarView.setBackgroundColor(Color.WHITE);
//                        TextView textView = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);
//                        textView.setTextColor(textColor);
                        snackbar.show();

                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                finish();
                            }
                        }, 1000);


                    }
                });

                alertbuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.w("ShowImage","Don't Delete");
                    }
                });

                AlertDialog alertDialog=alertbuilder.create();
                alertDialog.show();
                return true;
            }
        });





    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            firebaseDatabase=FirebaseDatabase.getInstance();
            modReference=firebaseDatabase.getReference("modified");
            String key=modReference.push().getKey();
            modReference.child(key).setValue(user);
            listreference=firebaseDatabase.getReference("images").child(user);
            listreference.child(UID).removeValue();

//                        Snackbar.make(coordinatorLayout,"Image deleted succesfully",Snackbar.LENGTH_SHORT).show();

            Snackbar snackbar;
            snackbar = Snackbar.make(coordinatorLayout, "Image deleted succesfully", Snackbar.LENGTH_SHORT);
            View snackBarView = snackbar.getView();
            snackBarView.setBackgroundColor(Color.WHITE);
//                        TextView textView = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);
//                        textView.setTextColor(textColor);
            snackbar.show();

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    finish();
                }
            }, 1000);



            return true;
        }

        return super.onOptionsItemSelected(item);
    }



}
