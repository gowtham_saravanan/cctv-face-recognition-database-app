package com.mbp.cctv;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.zip.Inflater;

public class MainActivity extends AppCompatActivity {
    Dialog a;
    EditText text;
    Button b1;
    String string;
    FirebaseDatabase fd,fd1;
    DatabaseReference adddf,retreivedf,deletedf,imagesdf;
    ArrayList<String> arrayList,arrayListUID;
    ArrayAdapter<String> arrayAdapter;
    ListView listView;
    AlertDialog alertDialog;
    AlertDialog.Builder alertbuilder;
    CoordinatorLayout coordinatorLayout;


    public static final String TAG="MainActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        LayoutInflater inflater=(LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        View view=inflater.inflate(R.layout.activity_main,null,false);
//        drawer.addView(view);


        setContentView(R.layout.activity_main);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        listView=(ListView)findViewById(R.id.camera_list);

        coordinatorLayout=(CoordinatorLayout)findViewById(R.id.clayout1);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                a=new Dialog(MainActivity.this);
                a.setContentView(R.layout.getnamelayout);
                text=(EditText)a.findViewById(R.id.editText);
                b1=(Button)a.findViewById(R.id.button);

                b1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        string=text.getText().toString();
                        fd=FirebaseDatabase.getInstance();
                        adddf=fd.getReference("list");
                        arrayAdapter.notifyDataSetChanged();

                        String key= adddf.push().getKey();
                        adddf.child(key).setValue(string);
                        a.dismiss();
                    }
                });

                a.show();

            }
        });


        fd1=FirebaseDatabase.getInstance();
        retreivedf=fd1.getReference("list");
        deletedf=fd1.getReference("delete");
        imagesdf=fd1.getReference("images");
        retreivedf.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                arrayList = new ArrayList<String>();
                arrayListUID=new ArrayList<String>();
                for (DataSnapshot ds1 : dataSnapshot.getChildren()) {
                    Log.i(TAG,"key = "+String.valueOf(ds1.getKey()));
                   arrayListUID.add(String.valueOf(ds1.getKey()));
                    arrayList.add(String.valueOf(ds1.getValue()));
                    Log.i(TAG, String.valueOf(ds1.getValue()));
//                    arrayAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1, arrayList);

                    arrayAdapter = new ArrayAdapter<String>
                            (MainActivity.this, android.R.layout.simple_list_item_1, arrayList){
                        @Override
                        public View getView(int position, View convertView, ViewGroup parent){
                            // Get the Item from ListView
                            View view = super.getView(position, convertView, parent);

                            // Initialize a TextView for ListView each Item
                            TextView tv = (TextView) view.findViewById(android.R.id.text1);

                            tv.setTextSize(18);
                            // Set the text color of TextView (ListView Item)
                            tv.setTextColor(Color.BLACK);

                            // Generate ListView Item using TextView
                            return view;
                        }
                    };

                    listView.setAdapter(arrayAdapter);
                    arrayAdapter.notifyDataSetChanged();
                }

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        //Log.i(TAG, String.valueOf(listView.getItemAtPosition(position)));
                        Intent intent=new Intent(MainActivity.this,Main2Activity.class);
                        intent.putExtra("user",String.valueOf(listView.getItemAtPosition(position)));
                        startActivity(intent);

                    }
                });

                listView.setLongClickable(true);
                listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                        alertbuilder=new AlertDialog.Builder(MainActivity.this);
                        String name=String.valueOf(listView.getItemAtPosition(position));
                        alertbuilder.setMessage("Do you want to delete "+name);
                        alertbuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Log.w(getLocalClassName(),"Delete");
                                retreivedf.child(arrayListUID.get(position)).removeValue();

                                String key=deletedf.push().getKey();
                                deletedf.child(key).setValue(arrayList.get(position));

                                String name=String.valueOf(listView.getItemAtPosition(position));
                                imagesdf.child(name).removeValue();


                                Snackbar snackbar;
                                snackbar = Snackbar.make(coordinatorLayout, name+ " removed from the database", Snackbar.LENGTH_SHORT);
                                View snackBarView = snackbar.getView();
                                snackBarView.setBackgroundColor(Color.WHITE);
                                snackbar.show();


                            }
                        });

                        alertbuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Log.w(getLocalClassName(),"Don't Delete");


                            }
                        });

                        alertDialog=alertbuilder.create();
                        alertDialog.show();

                        return  true;
                    }
                });


            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });







    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_predictions, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent=new Intent(MainActivity.this,About.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
