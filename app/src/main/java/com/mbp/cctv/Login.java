package com.mbp.cctv;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends AppCompatActivity {
    EditText user,pass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
       user=(EditText)findViewById(R.id.Username);
       pass=(EditText)findViewById(R.id.Password);
        Button button=(Button)findViewById(R.id.login);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Username=user.getText().toString();
                String Password=pass.getText().toString();
                if(!Username.isEmpty() && !Password.isEmpty()){
                    if(Username.equals("admin") && Password.equals("password")){
                        Toast.makeText(getApplicationContext(),"Login Successful",Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(Login.this,Selection.class);
                        startActivity(intent);
                    }else {
                        Toast.makeText(getApplicationContext(),"Invalid Credentials",Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });




    }
}
