package com.mbp.cctv;

public class Images {
    public String ImageName;
   public String ImageUrl;

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }

    public void setImageName(String name) {
        ImageName = name;
    }

    public String getImageName() {
        return ImageName;
    }

    public String getImageUrl() {
        return ImageUrl;
    }
}
