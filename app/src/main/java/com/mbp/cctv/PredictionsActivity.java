package com.mbp.cctv;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class PredictionsActivity extends AppCompatActivity  {
    String camera;

    private SectionsPagerAdapter mSectionsPagerAdapter;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    private ViewPager mViewPager;
    List<String> dates,pred;
    public List<String> rdates;
    public List<List<String>> rpred;


Map<String,List<String>> datamap=new HashMap<String, List<String>>();

    String filename="predictions_data.txt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_predictions);

//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.

        rdates=new ArrayList<>();
        rpred=new ArrayList<>();

        firebaseDatabase=FirebaseDatabase.getInstance();
        Bundle bundle=getIntent().getExtras();
        camera= bundle != null ? bundle.getString("camera") : "default";

        databaseReference=firebaseDatabase.getReference("predictions").child(camera);

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                dates=new ArrayList<>();

                for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                    dates.add(dataSnapshot1.getKey());
                    pred=new ArrayList<>();
                    for(DataSnapshot dataSnapshot2:dataSnapshot1.getChildren()){

                        String time=dataSnapshot2.getKey();
                        String pre=String.valueOf(dataSnapshot2.getValue());
//                        Log.i(getLocalClassName(),"Time = "+time);
//                        Log.i(getLocalClassName(),"Prediction = "+pre);
                        pred.add(time+"    "+pre);
                    }

                    datamap.put(dataSnapshot1.getKey(),pred);
//                    mSectionsPagerAdapter.notifyDataSetChanged();
                }

                saveArray(dates);
                rdates=loadArray();
                Collections.reverse(rdates);

                mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

                // Set up the ViewPager with the sections adapter.
                mViewPager = (ViewPager) findViewById(R.id.container);
                mViewPager.setAdapter(mSectionsPagerAdapter);




            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });







    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_predictions, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    /**
     * A placeholder fragment containing a simple view.
     */


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);

        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).

            return PlaceholderFragment.newInstance(position,camera,rdates.get(position));
        }

        @Override
        public int getCount() {
            // Show 3 total pages.


            return rdates.size();
        }
    }


    public  boolean saveArray(List<String> sKey)
    {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(PredictionsActivity.this);
        SharedPreferences.Editor mEdit1 = sp.edit();
        /* sKey is an array */
        mEdit1.putInt("Status_size", sKey.size());

        for(int i=0;i<sKey.size();i++)
        {
            mEdit1.remove("Status_" + i);
            mEdit1.putString("Status_" + i, sKey.get(i));
        }

        return mEdit1.commit();
    }



    public List<String> loadArray()
    {
        SharedPreferences mSharedPreference1 =   PreferenceManager.getDefaultSharedPreferences(PredictionsActivity.this);
        List<String> stringList=new ArrayList<>();
        stringList.clear();
        int size = mSharedPreference1.getInt("Status_size", 0);

        for(int i=0;i<size;i++)
        {
            stringList.add(mSharedPreference1.getString("Status_" + i, null));

        }

        return stringList;

    }

}
