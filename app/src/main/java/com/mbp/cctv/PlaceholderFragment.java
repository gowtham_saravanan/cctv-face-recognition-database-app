package com.mbp.cctv;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mbp.cctv.PredictionsActivity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class PlaceholderFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    List <String> pred;
    List<String> time;
    private static final String ARG_SECTION_NUMBER = "section_number";
    ListView listView;
    String camera,da;
    TableLayout stk;
    TextView results;
    public PlaceholderFragment() {

    }


    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static PlaceholderFragment newInstance(int sectionNumber, String camera,String date) {


        PlaceholderFragment fragment = new PlaceholderFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        args.putString("Date",date);
        args.putString("Camera",camera);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {



        final View rootView = inflater.inflate(R.layout.fragment_predictions, container, false);
         TextView date = (TextView) rootView.findViewById(R.id.date_text);


        Toolbar actionBar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(actionBar);


        final EditText search_edit=(EditText)rootView.findViewById(R.id.search);
        ImageButton search_button=(ImageButton)rootView.findViewById(R.id.searchbutton);
         stk = (TableLayout) rootView.findViewById(R.id.tablelayout);

         results=(TextView)rootView.findViewById(R.id.no_results);


        search_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stk.removeAllViews();
                display_predictions(search_edit.getText().toString());

            }
        });

        ImageView back=(ImageView)rootView.findViewById(R.id.imageView4);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

       da=getArguments().getString("Date");
       date.setText(da);
       camera=getArguments().getString("Camera");
       int pos=getArguments().getInt(ARG_SECTION_NUMBER);

       display_predictions("all");




        return rootView;
    }



    public  void display_predictions(final String search){

        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();


        DatabaseReference databaseReference = firebaseDatabase.getReference("predictions").child(camera).child(da);

        databaseReference.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {




                TableRow tbrow0 = new TableRow(getActivity());
                tbrow0.setPadding(50,10,50,10);
                tbrow0.setGravity(Gravity.CENTER);
                TextView tv0 = new TextView(getActivity());
                tv0.setText("           Time             ");
                tv0.setAllCaps(true);
                tv0.setTextColor(Color.BLACK);
                tv0.setTextSize(20);
                tv0.setTypeface(tv0.getTypeface(), Typeface.BOLD);
                tbrow0.addView(tv0);



                TextView tv1 = new TextView(getActivity());
                tv1.setText("           Prediction        ");
                tv1.setTextColor(Color.BLACK);
                tv1.setAllCaps(true);
                tv1.setTextSize(20);
                tv1.setTypeface(tv0.getTypeface(), Typeface.BOLD);
                tbrow0.addView(tv1);
                stk.addView(tbrow0);
                pred=new ArrayList<>();
                time=new ArrayList<String>();

                for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()) {

                    String time = String.valueOf(dataSnapshot1.child("time").getValue());
                    String pred = String.valueOf(dataSnapshot1.child("pred").getValue());
                    String com = time + pred;
                    if (search.equals("all")) {

                        TableRow tbrow = new TableRow(getContext());
                        tbrow.setGravity(Gravity.CENTER);
                        tbrow.setPadding(50, 10, 50, 10);
                        TextView t1v = new TextView(getContext());
                        t1v.setText(time);
                        t1v.setTextColor(Color.BLACK);
                        t1v.setGravity(Gravity.CENTER);
                        t1v.setTextSize(20);
                        tbrow.addView(t1v);
                        TextView t2v = new TextView(getContext());
                        t2v.setText(pred);
                        t2v.setTextColor(Color.BLACK);
                        t2v.setGravity(Gravity.CENTER);
                        t2v.setTextSize(20);
                        tbrow.setDividerPadding(20);
                        tbrow.addView(t2v);


                        stk.addView(tbrow);

                        results.setVisibility(View.INVISIBLE);


//                    mSectionsPagerAdapter.notifyDataSetChanged();
                    }
                    else{
                        int cnt=0;
                        if(com.contains(search)){
                            cnt=cnt+1;

                            TableRow tbrow = new TableRow(getContext());
                            tbrow.setGravity(Gravity.CENTER);
                            tbrow.setPadding(50, 10, 50, 10);
                            TextView t1v = new TextView(getContext());
                            t1v.setText(time);
                            t1v.setTextColor(Color.BLACK);
                            t1v.setGravity(Gravity.CENTER);
                            t1v.setTextSize(20);
                            tbrow.addView(t1v);
                            TextView t2v = new TextView(getContext());
                            t2v.setText(pred);
                            t2v.setTextColor(Color.BLACK);
                            t2v.setGravity(Gravity.CENTER);
                            t2v.setTextSize(20);
                            tbrow.setDividerPadding(20);
                            tbrow.addView(t2v);


                            stk.addView(tbrow);




                        }
                        if(cnt==0){
                            results.setVisibility(View.VISIBLE);
                        }
                        else{
                            results.setVisibility(View.INVISIBLE);
                        }

                    }
                }





            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });




    }
}